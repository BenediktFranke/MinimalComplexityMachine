import pandas as pd
from functools import reduce

dset_paper = pd.read_excel("../experiments/datasets_paper_Scores.xlsx")
dset_mcm_linear = pd.read_hdf("../experiments/results_mcm_linear.h5")
dset_svm_linear = pd.read_hdf("../experiments/results_svm_linear.h5")
dset_mcm_kernel = pd.read_hdf("../experiments/results_mcm.h5")
dset_svm_kernel = pd.read_hdf("../experiments/results_svm.h5")

dset_paper.columns = map(lambda x: x.lower(), dset_paper.columns)
dset_paper.loc[dset_paper['dataset'] == 'seed', 'dataset'] = 'seeds'
dset_paper.loc[dset_paper['dataset'] == 'ipld', 'dataset'] = 'ilpd'
dset_paper.loc[dset_paper['dataset'] == 'horsecolic', 'dataset'] = 'horsecolic_train'
dset_paper.loc[dset_paper['dataset'] == 'hayes', 'dataset'] = 'hayes-roth'
dset_paper.loc[dset_paper['dataset'] == 'Wholesale customers', 'dataset'] = 'Wholesale customers data'
dfs_kernel = [(dset_paper, ''), (dset_svm_kernel, 'svm'), (dset_mcm_kernel, 'mcm')]
dfs_linear = [(dset_paper, ''), (dset_svm_linear, 'svm'), (dset_mcm_linear, 'mcm')]
df_kernel = reduce(lambda left, right: pd.merge(left, right[0], on='dataset', how='outer', suffixes=('', right[1])),
                   dfs_kernel, pd.DataFrame({'dataset': []}))
df_linear = reduce(lambda left, right: pd.merge(left, right[0], on='dataset', how='outer', suffixes=('', right[1])),
                   dfs_linear, pd.DataFrame({'dataset': []}))

df_kernel.drop([x for x in df_kernel.columns if "_lin_" in x], axis=1, inplace=True)
df_linear.drop([x for x in df_linear.columns if "_ker_" in x], axis=1, inplace=True)

df_kernel.loc[df_kernel['dataset'] == 'Wholesale customers data', 'dataset'] = "wholesale"
df_linear.loc[df_linear['dataset'] == 'Wholesale customers data', 'dataset'] = "wholesale"

df_kernel[['mcm_ker_acc', 'svm_ker_acc', 'mcm_ker_std', 'svm_ker_std']] = \
    df_kernel[['mcm_ker_acc', 'svm_ker_acc', 'mcm_ker_std', 'svm_ker_std']].apply(lambda x: x / 100)

df_linear[['mcm_lin_acc', 'svm_lin_acc', 'mcm_lin_std', 'svm_lin_std']] = \
    df_linear[['mcm_lin_acc', 'svm_lin_acc', 'mcm_lin_std', 'svm_lin_std']].apply(lambda x: x / 100)

with open("tbl_kernel_acc.tex", 'w+') as fp:
    df_kernel.to_latex(fp, columns=["dataset", "mcm_ker_acc", "mcm_ker_std", "acc_meanmcm", "acc_stdmcm"],
                       float_format="{:0.2f}".format, index=False,
                       header=["dataset", "MATLAB-MCM (mean)", "MATLAB-MCM (std)", "mcmpy (acc)", "mcmpy (std)"])

with open("tbl_linear_acc.tex", 'w+') as fp:
    df_linear.to_latex(fp, columns=["dataset", "mcm_lin_acc", "mcm_lin_std", "acc_meanmcm", "acc_stdmcm"],
                       float_format="{:0.2f}".format, index=False,
                       header=["dataset", "MATLAB-MCM (mean)", "MATLAB-MCM (std)", "mcmpy (acc)", "mcmpy (std)"])

with open("tbl_kernel_sv.tex", 'w+') as fp:
    df_kernel.to_latex(fp, columns=["dataset", "mcm_ker_sv_mean", "mcm_ker_sv_std", "sv_meanmcm", "sv_stdmcm"],
                       float_format="{:0.2f}".format, index=False,
                       header=["dataset", "MATLAB-MCM (mean)", "MATLAB-MCM (std)", "mcmpy (acc)", "mcmpy (std)"])

from mcmpy import MCMClassifier
import pandas as pd
from sklearn.preprocessing import StandardScaler

#mcm = MCMClassifier(C=0.20042723493347347, kernel='gauss', gamma=2.359716332611933, verbose=2, algorithm='dual')
mcm = MCMClassifier(C=0.20042723493347347, kernel='gauss', gamma=3, verbose=2)
df = pd.read_hdf("processed.h5", key="transfusion")

X = df.iloc[:, :-1].values
y = df.iloc[:, -1].values

sc = StandardScaler()
X = sc.fit_transform(X)

mcm.fit(X, y)
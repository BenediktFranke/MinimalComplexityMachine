import itertools
import logging

import numpy as np
from joblib import Parallel, delayed
from joblib._memmapping_reducer import has_shareable_memory
from scipy.io import loadmat
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from mcmpy import MCMClassifier

def outer_loop(test_id):
    # try all hyperparameters
    validation_id = (test_id + 5) % len(np.unique(ids))
    test_mask = ids == test_id
    val_mask = ids == validation_id
    train_mask = np.logical_and(ids != test_id, ids != validation_id)
    best_estimator = {'params': None, 'score': 0}
    for params in grid_ls:
        logging.info(f"test id: {test_id}, val id: {validation_id}, params: {str(params)}")
        pipe.set_params(**params)
        pipe.fit(X[train_mask], y[train_mask])
        val_score = accuracy_score(y[validation_id], pipe.predict(X[val_mask]))
        logging.info(f"validation score: {val_score}")
        if val_score > best_estimator['score']:
            best_estimator['score'] = val_score
            best_estimator['params'] = params

    # evaluate on test set
    refit_mask = ids != test_id
    pipe.set_params(**best_estimator['params'])
    pipe.fit(X[refit_mask])
    cv_score = accuracy_score(y[test_mask], pipe.predict(X[test_mask]))
    cv_params[test_id] = best_estimator['params']
    cv_scores[test_id] = cv_score
    logging.info(f"test id: {test_id}, achieved test score: {cv_score}")

logging.basicConfig(format='%(asctime)s  %(message)s', level=logging.INFO)

# load the data
data = loadmat("../experiments/BioVid.mat")
ids = data["ID"]
X = data["data"]
y = np.squeeze(data["labels"])

# prepare a results file
res_file = "pain_results.log"

# only keep classes 0 and 4
inds = np.where((y == 0) | (y == 4))[0]
ids = np.squeeze(ids[inds])
X = X[inds]
y = y[inds]

# only keep SCL
# X = X[:, np.where(np.isin(data['channelInfo'], [5,6,7]))[0]]


# prepare pipeline
pipe = Pipeline([("pca", PCA(whiten=True)), ("scale", StandardScaler()), ("mcm", MCMClassifier())])

# prepare param grid
grid = {
    'mcm__C': np.arange(20, 150, 1),
    'mcm__kernel': ['gauss'],
    'mcm__gamma': np.arange(0.01, 3, 0.1),
    'mcm__verbose': [0],
    'pca__n_components': np.arange(5, 20, 1)
}

# dict of lists to list of dicts
grid_ls = [{key: val for key, val in zip(sorted(grid.keys()), tup)}
           for tup in itertools.product(*[grid[key] for key in sorted(grid.keys())])]

logging.info(f"doing nested cv with {len(np.unique(ids))} outer splits and "
             f"{len(grid_ls)} parameter combinations, totalling {len(np.unique(ids)) * len(grid_ls)} loops")

cv_params = [{}] * len(np.unique(ids))
cv_scores = [0] * len(cv_params)

Parallel(n_jobs=15)(delayed(has_shareable_memory)(outer_loop(test_id)) for test_id in np.unique(ids))

logging.info(f"FINAL: cv score: {np.mean(cv_scores)}")
with open(res_file, 'a+') as fp:
    fp.write("------------EXPERIMENT------------\n")
    fp.write("!crossvalidated!")
    fp.write("Pipeline:\n")
    fp.write(str(pipe.named_steps))
    fp.write("\nparams:\n")
    fp.write(str(cv_params))
    fp.write("\naccuracy: {}\n".format(np.mean(cv_scores)))

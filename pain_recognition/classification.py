from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import GridSearchCV, LeaveOneGroupOut
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
import numpy as np
from mcmpy import MCMClassifier
from scipy.io import loadmat

# load the data
data = loadmat("../experiments/BioVid.mat")
ids = data["ID"]
X = data["data"]
y = np.squeeze(data["labels"])

# prepare a results file
res_file = "pain_results.log"

# only keep classes 0 and 4
inds = np.where((y == 0) | (y == 4))[0]
ids = ids[inds]
X = X[inds]
y = y[inds]

# only keep SCL
X = X[:, np.where(np.isin(data['channelInfo'], [5,6,7]))[0]]

# hold one subject out as test set (TODO: should we use nested CV instead?)
test_subject = 42
test_mask = np.where(ids == test_subject)[0]
train_mask = np.where(ids != test_subject)[0]
X_train = X[train_mask]
X_test = X[test_mask]
y_train = y[train_mask]
y_test = y[test_mask]
ids_train = np.squeeze(ids[train_mask])

# prepare pipeline
pipe = Pipeline([("pca", PCA(whiten=True)), ("scale", StandardScaler()), ("mcm", MCMClassifier())])

# prepare param grid
grid = {
    'mcm__C': np.arange(0.25, 0.35, 0.01),
    'mcm__kernel': ['gauss'],
    'mcm__gamma': np.arange(0.001, 0.01, 0.001),
    'mcm__verbose': [0],
    'pca__n_components': [19, 20, 21],
    'mcm__algorithm': ['dual']
}
# leave-one-subject-out CV
# grp = LeaveOneGroupOut()

# random search
search = GridSearchCV(pipe, grid, n_jobs=15, cv=2, verbose=2)
search.fit(X_train, y_train, ids_train)

# test
y_pred = search.predict(X_test)
acc = accuracy_score(y_test, y_pred)
#conf = confusion_matrix(y_test, y_pred)

# results
print("RESULTS:")
#print(conf)
print(acc)

with open(res_file, 'a+') as fp:
    fp.write("------------EXPERIMENT------------\n")
    fp.write("Pipeline:\n")
    fp.write(str(pipe.named_steps))
    fp.write("\nparams:\n")
    fp.write(str(search.best_params_))
    fp.write("\naccuracy: {}\n".format(acc))

from matplotlib import pyplot as plt
import numpy as np
from sklearn.svm import SVC

from mcmpy import MCMClassifier
from matplotlib import rcdefaults, use

rcdefaults()

data = np.array([[0, 1, 2, 3, 4, 6, 7, 8, 9], [0,0,0,0,0,1,1,1,1]])
X = data.transpose()
y = data[1]

mcm = MCMClassifier(kernel="linear", C=1000).fit(X, y)
svm = SVC(kernel="linear", C=1000).fit(X, y)

#fig = plt.figure(figsize=(14, 6), dpi=80, facecolor='w', edgecolor='k')
#ax1 = fig.add_subplot(121)
#ax2 = fig.add_subplot(122)

plt.scatter(X[:, 0], X[:, 1], c=['r' if e else 'b' for e in y])

line = lambda x, w, b: - (w[0]*x + b)/w[1]

xs = np.arange(-2, 10.1, 0.1)
plt.plot(xs, line(xs, mcm.w[0], mcm.b[0]))
plt.xlim((-1, 10))
plt.ylim((-5, 6))
plt.xticks([])
plt.yticks([])
plt.show(block=True)

plt.scatter(X[:, 0], X[:, 1], c=['r' if e else 'b' for e in y])
plt.plot(xs, line(xs, svm.coef_[0], svm.intercept_[0]))
plt.xlim((-1, 10))
plt.ylim((-5, 6))
plt.xticks([])
plt.yticks([])
plt.show()

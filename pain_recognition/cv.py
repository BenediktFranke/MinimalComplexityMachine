from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import GridSearchCV, LeaveOneGroupOut, cross_validate
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
import numpy as np
from mcmpy import MCMClassifier
from scipy.io import loadmat

# load the data
data = loadmat("../experiments/BioVid.mat")
ids = data["ID"]
X = data["data"]
y = np.squeeze(data["labels"])

# prepare a results file
res_file = "pain_results.log"

# only keep classes 0 and 4
inds = np.where((y == 0) | (y == 4))[0]
ids = np.squeeze(ids[inds])
X = X[inds]
y = y[inds]

# only keep SCL
#X = X[:, np.where(np.isin(data['channelInfo'], [5,6,7]))[0]]


# prepare pipeline
pipe = Pipeline([("pca", PCA(whiten=True)), ("scale", StandardScaler()), ("mcm", MCMClassifier())])

# prepare param grid
grid = {
    'mcm__C': np.arange(30, 100, 5),
    'mcm__kernel': ['gauss'],
    'mcm__gamma': np.arange(0.01, 2, 0.2),
    'mcm__verbose': [0],
    'pca__n_components': [18, 20, 22, 25]
}

# random search
search = GridSearchCV(pipe, grid, n_jobs=15, cv=2, verbose=2)
results = cross_validate(search, X, y, cv=LeaveOneGroupOut(), groups=ids, n_jobs=1, return_estimator=True)

# results
print("RESULTS:")
print(results['test_score'].mean())

with open(res_file, 'a+') as fp:
    fp.write("------------EXPERIMENT------------\n")
    fp.write("!crossvalidated!")
    fp.write("Pipeline:\n")
    fp.write(str(pipe.named_steps))
    fp.write("\nparams:\n")
    fp.write(str(list(map(lambda x: x.best_params_, results['estimator']))))
    fp.write("\naccuracy: {}\n".format(results['test_score'].mean()))

<h2>Introduction</h2>

This is a sklearn-compatible python implementation of the MinimalComplexityMachine, as presented in https://www.sciencedirect.com/science/article/pii/S0925231214010194.
The main class is found under "mcmpy/MCM.py".

The linear solver is based on the Gurobi-Framework, and an active license is required to run this.
More information under https://www.gurobi.com/.

<h2> Installation</h2>

The following libraries are hard dependencies:

- conda (native python is not supported due to gurobi dependency)
- numpy
- sklearn
- gurobipy, the python binding of the gurobi framework (https://www.gurobi.com/). You need to register for a educational license. <br/>

In an active conda envoirment, change into the root of this repository and execute:

<code>pip install dist/mcmpy-1.3.3-py3-none-any.whl</code>

<h2>Documentation</h2>

The documentation can currently be found in the source file "mcmpy/MCM.py". Usage examples can be found in the file "examples.py".

<h2> Project Structure </h2>

- data: Datasets used in the original Paper for performance comparison.
- dist: contains pip installable wheel
- experiments: Applications of the MCM on example data/ datasets from the "data"-folder
- mcmpy: The package with the MCM-implementation
- pain_recognition: Application of mcmpy on the "BioVid"-Dataset
- presentation: contains the PowerPoint-Presentation of this bachelor's thesis
- visualization: various plotting scripts
- examples.py: Usage examples of the MCM-implementation
- setup.py: Script to generate a pip-installable .whl file. Depends on the python libraries "setuptools" and "wheel"
import json

import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline

from mcmpy import MCMClassifier
from sklearn.model_selection import GridSearchCV, cross_validate
from sklearn.preprocessing import StandardScaler


<<<<<<< HEAD
df = pd.read_hdf("processed.h5", key="ecoli")
=======
df = pd.read_hdf("processed.h5", key="glass")
>>>>>>> 281151531756b60fd449c05e634c6097509a5ca8
X = df.iloc[:, :-1].values
y = df.iloc[:, -1].values

grid = {
    "cls__C": np.arange(30, 100, 3),
    "cls__kernel": ["gauss"],
    "cls__gamma": np.arange(0.1, 10, 0.1),
    "cls__algorithm": ["barrier"],
    "cls__tol": [1e-6, 1e-9]
}

pipe = Pipeline([('scale', StandardScaler()), ('cls', MCMClassifier(verbose=1))])
search = GridSearchCV(pipe, grid, n_jobs=8, cv=4, verbose=0)
results = cross_validate(search, X, y, cv=5, n_jobs=1, return_estimator=True, verbose=1)

print(results['test_score'])
print(results['test_score'].mean())
param_sets = [e.best_params_ for e in results['estimator']]
print(param_sets)

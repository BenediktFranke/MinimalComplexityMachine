import pandas as pd
from functools import reduce
import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib import rc
import numpy as np


not_plot = ["balance_scale", "horsecolic_train", "tae"
            "hepatitis", "glass", "hayes-roth", "haberman"]

rc('text', usetex=False)
dset_paper = pd.read_excel("../experiments/datasets_paper_Scores.xlsx")
dset_mcm_kernel = pd.read_hdf("../experiments/results_mcm.h5")
dset_svm_kernel = pd.read_hdf("../experiments/results_svm.h5")


dset_paper.columns = map(lambda x: x.lower(), dset_paper.columns)
dset_paper.loc[dset_paper['dataset'] == 'seed', 'dataset'] = 'seeds'
dset_paper.loc[dset_paper['dataset'] == 'ipld', 'dataset'] = 'ilpd'
dset_paper.loc[dset_paper['dataset'] == 'horsecolic', 'dataset'] = 'horsecolic_train'
dset_paper.loc[dset_paper['dataset'] == 'hayes', 'dataset'] = 'hayes-roth'
dset_paper.loc[dset_paper['dataset'] == 'Wholesale customers', 'dataset'] = 'Wholesale customers data'
dfs_kernel = [(dset_paper, ''), (dset_mcm_kernel, 'mcm'), (dset_svm_kernel, 'svm')]
df_kernel = reduce(lambda left, right: pd.merge(left, right[0], on='dataset', how='outer', suffixes=('', right[1])),
                   dfs_kernel, pd.DataFrame({'dataset': []}))


df_kernel.drop([x for x in df_kernel.columns if "_lin_" in x], axis=1, inplace=True)

df_kernel.loc[df_kernel['dataset'] == 'Wholesale customers data', 'dataset'] = "wholesale"

df_kernel[['mcm_ker_acc', 'svm_ker_acc', 'mcm_ker_std', 'svm_ker_std']] = \
    df_kernel[['mcm_ker_acc', 'svm_ker_acc', 'mcm_ker_std', 'svm_ker_std']].apply(lambda x: x / 100)

df1 = pd.melt(df_kernel[['dataset', 'mcm_ker_acc', 'acc_mean']], id_vars=['dataset'], value_name="acc_mean")\
    .sort_values(['dataset'])

df1 = df1[np.logical_not(np.isin(df1['dataset'], not_plot))]
df1 = df1[df1["dataset"] != "tae"]
df1 = df1[df1["dataset"] != "hepatitis"]

df1['variable'] = ["mcmpy" if v == "acc_mean" else "original" for v in df1["variable"]]
sns.set_style("whitegrid")
g = sns.barplot(x='dataset', y='acc_mean', hue='variable', data=df1)
plt.ylim((0.3, 1.0))
plt.ylabel("accuracy")
plt.xticks(rotation=90)
g.legend(loc='center left', bbox_to_anchor=(1.01, 0.5), ncol=1)

plt.show()


df2 = pd.melt(df_kernel[['dataset', 'acc_meansvm', 'acc_mean']], id_vars=['dataset'], value_name="acc_mean")\
    .sort_values(['dataset'])

df2 = df2[np.logical_not(np.isin(df2['dataset'], not_plot))]
df2 = df2[df2["dataset"] != "tae"]
df2 = df2[df2["dataset"] != "hepatitis"]

df2['variable'] = ["mcmpy" if v == "acc_mean" else "SVC" for v in df2["variable"]]
sns.set_style("whitegrid")
g = sns.barplot(x='dataset', y='acc_mean', hue='variable', data=df2)
plt.ylim((0.3, 1.0))
plt.ylabel("accuracy")
plt.xticks(rotation=90)
g.legend(loc='center left', bbox_to_anchor=(1.01, 0.5), ncol=1)

plt.show()
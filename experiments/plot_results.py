from functools import reduce

import matplotlib
from tikzplotlib import save
import pylab as pl
import pandas as pd
#from latexipy import latexify

#latexify()


def plot_grouped_acc(latex=True):

    dset_paper = pd.read_excel("datasets_paper_Scores.xlsx")
    dset_mcm_linear = pd.read_hdf("results_mcm_linear.h5")
    dset_svm_linear = pd.read_hdf("results_svm_linear.h5")
    dset_mcm_kernel = pd.read_hdf("results_mcm.h5")
    dset_svm_kernel = pd.read_hdf("results_svm.h5")

    dset_paper.columns = map(lambda x: x.lower(), dset_paper.columns)
    dset_paper.loc[dset_paper['dataset'] == 'seed', 'dataset'] = 'seeds'
    dset_paper.loc[dset_paper['dataset'] == 'ipld', 'dataset'] = 'ilpd'
    dset_paper.loc[dset_paper['dataset'] == 'horsecolic', 'dataset'] = 'horsecolic_train'
    dset_paper.loc[dset_paper['dataset'] == 'hayes', 'dataset'] = 'hayes-roth'
    dset_paper.loc[dset_paper['dataset'] == 'Wholesale customers', 'dataset'] = 'Wholesale customers data'
    dfs_kernel = [(dset_paper, ''), (dset_svm_kernel, 'svm'), (dset_mcm_kernel, 'mcm')]
    dfs_linear = [(dset_paper, ''), (dset_svm_linear, 'svm'), (dset_mcm_linear, 'mcm')]
    df_kernel = reduce(lambda left, right: pd.merge(left, right[0], on='dataset', how='outer', suffixes=('', right[1])),
                       dfs_kernel, pd.DataFrame({'dataset': []}))
    df_linear = reduce(lambda left, right: pd.merge(left, right[0], on='dataset', how='outer', suffixes=('', right[1])),
                       dfs_linear, pd.DataFrame({'dataset': []}))
                       
    df_kernel.loc[df_kernel['dataset'] == 'horsecolic_train', 'dataset'] = "horsecolic"
    df_linear.loc[df_linear['dataset'] == 'horsecolic_train', 'dataset'] = "horsecolic"
    df_kernel.loc[df_kernel['dataset'] == 'house_voters', 'dataset'] = "house voters"
    df_linear.loc[df_linear['dataset'] == 'house_voters', 'dataset'] = "house voters"
    df_kernel.loc[df_kernel['dataset'] == 'balance_scale', 'dataset'] = "balance scale"
    df_linear.loc[df_linear['dataset'] == 'balance_scale', 'dataset'] = "balance scale"

    df_kernel.drop([x for x in df_kernel.columns if "_lin_" in x], axis=1, inplace=True)
    df_linear.drop([x for x in df_linear.columns if "_ker_" in x], axis=1, inplace=True)

    df_kernel.loc[df_kernel['dataset'] == 'Wholesale customers data', 'dataset'] = "wholesale"
    df_linear.loc[df_linear['dataset'] == 'Wholesale customers data', 'dataset'] = "wholesale"

    df_kernel[['mcm_ker_acc', 'svm_ker_acc', 'mcm_ker_std', 'svm_ker_std']] = \
        df_kernel[['mcm_ker_acc', 'svm_ker_acc', 'mcm_ker_std', 'svm_ker_std']].apply(lambda x: x / 100)

    df_linear[['mcm_lin_acc', 'svm_lin_acc', 'mcm_lin_std', 'svm_lin_std']] = \
        df_linear[['mcm_lin_acc', 'svm_lin_acc', 'mcm_lin_std', 'svm_lin_std']].apply(lambda x: x / 100)

    ax_ker_paper = df_kernel[['dataset', 'mcm_ker_acc', 'acc_meanmcm']]\
        .plot.bar(x='dataset', ylim=(0.4, 1), title="kernel results", width=0.8)
    ax_lin_paper = df_linear[['dataset', 'mcm_lin_acc', 'acc_meanmcm']]\
        .plot.bar(x='dataset', ylim=(0.4, 1), title="linear results", width=0.8)

    ax_ker_svm = df_kernel[['dataset', 'acc_mean', 'acc_meanmcm']] \
        .plot.bar(x='dataset', ylim=(0.4, 1), title="kernel results", width=0.8)
    ax_lin_svm = df_linear[['dataset', 'acc_mean', 'acc_meanmcm']] \
        .plot.bar(x='dataset', ylim=(0.4, 1), title="linear results", width=0.8)

    legend_map_paper = ["mcm paper", "mcmpy"]
    legend_map_svm = ["sklearn SVM", "mcmpy"]

    ax_ker_paper.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    ax_lin_paper.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    ax_ker_svm.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    ax_lin_svm.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))

    [ax_ker_paper.get_legend().get_texts()[i].set_text(legend_map_paper[i]) for i in range(2)]
    [ax_lin_paper.get_legend().get_texts()[i].set_text(legend_map_paper[i]) for i in range(2)]
    [ax_ker_svm.get_legend().get_texts()[i].set_text(legend_map_svm[i]) for i in range(2)]
    [ax_lin_svm.get_legend().get_texts()[i].set_text(legend_map_svm[i]) for i in range(2)]

    if not latex:
        ax_ker_paper.figure.savefig("plots/kernel_paper_acc.png", dpi=300, bbox_inches='tight')
        ax_lin_paper.figure.savefig("plots/linear_paper_acc.png", dpi=300, bbox_inches='tight')
        ax_ker_svm.figure.savefig("plots/kernel_sklearn_acc.png", dpi=300, bbox_inches='tight')
        ax_lin_svm.figure.savefig("plots/linear_sklearn_acc.png", dpi=300, bbox_inches='tight')
    else:
        matplotlib.use('pgf')
        pl.figure(ax_ker_paper.figure.number)
        save("plots/kernel_paper_acc.tex")
        pl.figure(ax_lin_paper.figure.number)
        save("plots/linear_paper_acc.tex")
        pl.figure(ax_ker_svm.figure.number)
        save("plots/kernel_sklearn_acc.tex")
        pl.figure(ax_lin_svm.figure.number)
        save("plots/linear_sklearn_acc.tex")
        ax_ker_paper.figure.savefig("plots/kernel_paper_acc.pdf", dpi=300, bbox_inches='tight')
        ax_lin_paper.figure.savefig("plots/linear_paper_acc.pdf", dpi=300, bbox_inches='tight')
        ax_ker_svm.figure.savefig("plots/kernel_sklearn_acc.pdf", dpi=300, bbox_inches='tight')
        ax_lin_svm.figure.savefig("plots/linear_sklearn_acc.pdf", dpi=300, bbox_inches='tight')



def plot_grouped_sv(latex=True):

    dset_paper = pd.read_excel("datasets_paper_Scores.xlsx")
    dset_mcm_kernel = pd.read_hdf("results_mcm.h5")
    dset_svm_kernel = pd.read_hdf("results_svm.h5")

    dset_paper.columns = map(lambda x: x.lower(), dset_paper.columns)
    dset_paper.loc[dset_paper['dataset'] == 'seed', 'dataset'] = 'seeds'
    dset_paper.loc[dset_paper['dataset'] == 'ipld', 'dataset'] = 'ilpd'
    dset_paper.loc[dset_paper['dataset'] == 'horsecolic', 'dataset'] = 'horsecolic_train'
    dset_paper.loc[dset_paper['dataset'] == 'hayes', 'dataset'] = 'hayes-roth'
    dset_paper.loc[dset_paper['dataset'] == 'Wholesale customers', 'dataset'] = 'Wholesale customers data'
    dfs_kernel = [(dset_paper, ''), (dset_svm_kernel, 'svm'), (dset_mcm_kernel, 'mcm')]
    df_kernel = reduce(lambda left, right: pd.merge(left, right[0], on='dataset', how='outer', suffixes=('', right[1])),
                       dfs_kernel, pd.DataFrame({'dataset': []}))

    df_kernel.drop([x for x in df_kernel.columns if "_lin_" in x], axis=1, inplace=True)

    df_kernel.loc[df_kernel['dataset'] == 'Wholesale customers data', 'dataset'] = "wholesale"

    ax_ker = df_kernel[['dataset', 'mcm_ker_sv_mean', 'svm_ker_sv_mean', 'sv_mean', 'sv_meanmcm']]\
        .plot.bar(x='dataset', title="kernel support vectors", width=0.8)

    legend_map = ["mcm paper", "svm paper", "sklearn SVM", "mcmpy"]

    ax_ker.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))

    [ax_ker.get_legend().get_texts()[i].set_text(legend_map[i]) for i in range(4)]

    ax_ker.figure.savefig("kernel_sv.png", dpi=300, bbox_inches='tight')

    if not latex:
        ax_ker.figure.savefig("plots/kernel_sv.png", dpi=300, bbox_inches='tight')
    else:
        pl.figure(ax_ker.figure.number)
        #save("plots/kernel_sv.tex")
        matplotlib.use('pgf')
        ax_ker.figure.savefig("plots/kernel_sv.pdf", dpi=300, bbox_inches='tight')


def plot_grouped_h(latex=True):

    dset_paper = pd.read_excel("datasets_paper_Scores.xlsx")
    dset_mcm_linear = pd.read_hdf("results_mcm_linear.h5")

    dset_paper.columns = map(lambda x: x.lower(), dset_paper.columns)
    dset_paper.loc[dset_paper['dataset'] == 'seed', 'dataset'] = 'seeds'
    dset_paper.loc[dset_paper['dataset'] == 'ipld', 'dataset'] = 'ilpd'
    dset_paper.loc[dset_paper['dataset'] == 'horsecolic', 'dataset'] = 'horsecolic_train'
    dset_paper.loc[dset_paper['dataset'] == 'hayes', 'dataset'] = 'hayes-roth'
    dset_paper.loc[dset_paper['dataset'] == 'Wholesale customers', 'dataset'] = 'Wholesale customers data'
    dfs_linear = [(dset_paper, ''), (dset_mcm_linear, 'mcm')]
    df_linear = reduce(lambda left, right: pd.merge(left, right[0], on='dataset', how='outer', suffixes=('', right[1])),
                       dfs_linear, pd.DataFrame({'dataset': []}))

    df_linear.drop([x for x in df_linear.columns if "_ker_" in x], axis=1, inplace=True)

    df_linear.loc[df_linear['dataset'] == 'Wholesale customers data', 'dataset'] = "wholesale"

    ax_lin = df_linear[['dataset', 'mcm_lin_h_mean', 'hs_mean']]\
        .plot.bar(x='dataset', title="linear h-values", width=0.8)

    legend_map = ["mcm paper", "mcmpy"]

    ax_lin.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))

    [ax_lin.get_legend().get_texts()[i].set_text(legend_map[i]) for i in range(2)]

    ax_lin.figure.savefig("linear_h.png", dpi=300, bbox_inches='tight')

    if not latex:
        ax_lin.figure.savefig("plots/linear_h.png", dpi=300, bbox_inches='tight')
    else:
        pl.figure(ax_lin.figure.number)
        save("plots/linear_h.tex")


latex = True

plot_grouped_acc(latex)
plot_grouped_sv(latex)
plot_grouped_h(latex)

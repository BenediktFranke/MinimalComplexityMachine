import time
import warnings
from itertools import combinations
from types import FunctionType
from typing import Sequence, Union, List, Tuple, Callable, Dict

import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.metrics import accuracy_score


class MCMClassifier(BaseEstimator, ClassifierMixin):
    """
    A python implementation of the Minimal Complexity Machine
    from  (Jayadeva. ”Learning a hyperplane classifier by minimizing an exact bound on the VC dimension.”
    NEUROCOMPUTING 149 (2015): 683-689.)
    Based on the gurobi framework, https://www.gurobi.com/
    """

    def __init__(self, C: float = 1.0, kernel: Union[str, Callable] = 'gauss', gamma: float = 5.0,
                 degree: int = 2, coef0: float = 0, maxiter: int = 4000, tol: float = 5.0e-7, multiclass: str = 'ovr',
                 algorithm: str = 'auto', verbose: int = 0, **kwargs):
        """
        :param C: SVM-like C parameter

        :param kernel: kernel to be used, one of [linear, poly, gauss, sigmoid], or a function that accepts
                        two arrays as arguments and returns a float

        :param gamma: coefficient for gauss and sigmoid kernel

        :param degree: degree for poynomial kernel

        :param coef0: constant term for sigmoid kernel

        :param maxiter: hard limit on linprog iteration, None for maximum

        :param tol: tolerance of error for linprog

        :param multiclass: strategy used to binarize the problem. [ovr, ovo], Ignored if problem is binary

        :param algorithm: algorithm to solve the minimization problem. One of:
                    'primal' for primal simplex
                    'dual' for dual simplex
                    'barrier' for barrier method
                    'auto' for letting gurobi choose the algorithm it deems most appropriate

                WARNING: 'auto' will often result in multiple optimizations run in parallel and can therefore use a lot
                            of resources
        :param verbose: higher numbers mean more debug output

        :param kwargs: arguments passed to parent mcmpy

        """
        super().__init__(**kwargs)
        self.multiclass = multiclass
        self.coef0 = coef0
        self.gamma = gamma
        self.degree = degree
        self.kernel = kernel
        self.C = C
        self.maxiter = maxiter
        self.tol = tol
        self.verbose = verbose
        self.algorithm = algorithm
        self.K: Callable = self._kernel_linear
        self.n_samples: int = 0
        self.n_classes: int = 0
        self.Xs: np.ndarray = np.array([])
        self.ys: np.ndarray = np.array([])
        self.cls: List[Tuple[np.ndarray, float, np.ndarray, float]] = []
        self._fitted: bool = False
        self.ovo_combis = None
        self.classes: Dict = {}
        self.allowed_algorithms = ["primal", "dual", "barrier", "auto"]
        self.kernel_mat = None

    def fit(self, X: Sequence[Sequence[float]], y: Sequence[int]):
        """
        train classifier on given data

        :param X: feature vectors, 2d np.array

        :param y: target vector, 1d np array. Notice that the MCM is capable of dealing with
        categorical labels of arbitrary type, so passing an array of string labels is permitted,
        as well as numerical classes with missing values

        :return: self: fitted estimator

        """

        if not isinstance(X, np.ndarray):
            X = np.array(X)
        if not isinstance(y, np.ndarray):
            y = np.array(y)

        assert len(X.shape) == 2, "expected len(X.shape) = 2, but got {}. Restructure your data using " \
                                  "np.ndarray.reshape(-1, 1) if your data has a single feature or " \
                                  "np.ndarray.reshape(1, -1) if your data consists of a single point".format(X.shape)

        assert len(y.shape) == 1

        # param validation
        if self.kernel == "linear":
            self.K = self._kernel_linear
        elif self.kernel == "poly":
            self.K = self._kernel_poly
        elif self.kernel == "gauss":
            self.K = self._kernel_gauss
        elif self.kernel == "sigmoid":
            self.K = self._kernel_sigmoid
        elif isinstance(self.kernel, FunctionType):
            self.K = self.kernel
        else:
            raise ValueError("kernel not understood: {}".format(self.kernel))

        assert self.multiclass in ["ovr", "ovo"], "binarization strategy not understood: {}".format(self.multiclass)
        assert self.algorithm in self.allowed_algorithms, "algorithm not understood: {}".format(self.algorithm)

        self.algorithm = -1 if self.algorithm == "auto" else self.allowed_algorithms.index(self.algorithm)

        """
        if self.gamma == "auto":
            self.gamma = 1.0 / X.shape[1]
        elif self.gamma == "scale":
            self.gamma = 1.0 / (X.shape[1] * X.var())"""

        # attribute inferring
        self.n_samples = X.shape[0]
        y = self._map_classes(y)
        if self.verbose > 1:
            print("detected input classes: {}".format(self.classes))
        self.n_classes = len(self.classes)
        self.Xs, self.ys = self._binarize(X, y)

        start_time = int(round(time.time() * 1000))
        # the actual training
        solve = self._solve_optimization
        self.cls = [solve(X_p, y_p) for X_p, y_p in zip(self.Xs, self.ys)]
        end_time = int(round(time.time() * 1000))
        if self.verbose > 0:
            print("Training took {} milliseconds".format(end_time - start_time))

        # allow prediction and return fitted self as stated by sklearn standard
        self._fitted = True
        return self

    def predict(self, X: Union[Sequence[float], Sequence[Sequence[float]]], y=None) -> np.ndarray:
        """
        predict labels for given feature vectors

        :param X: feature vector(s), 1d or 2d np.array

        :param y: ignored (here for compabillity reasons)

        :return: np.array of shape (len(X),) with predicted labels (same dtype as the labels had when passed to fit)

        """
        if not self._fitted:
            raise RuntimeError("The classifier was not fitted prior to calling predict(). "
                               "Call fit at least once on the training data before attempting to predict.")

        if not isinstance(X, np.ndarray):
            X = np.array(X)

        assert len(X.shape) == 2, "expected len(X.shape) = 2, but got {}. Restructure your data using " \
                                  "np.ndarray.reshape(-1, 1) if your data has a single feature or " \
                                  "np.ndarray.reshape(1, -1) if your data consists of a single point".format(X.shape)

        if self.n_classes == 2:
            result = self._vote_binary(X)
        elif self.multiclass == "ovr":
            result = self._vote_ovr(X)
        elif self.multiclass == "ovo":
            result = self._vote_ovo(X)
        else:
            raise ValueError("binarization strategy not understood: {}".format(self.multiclass))
        # map class -1 to class 0
        if -1 in result:
            assert 0 not in result, "class -1 and class 0 were found in results, is this a regression task?\nResults: " \
                                    "{}".format(result)
            result[result == -1] = 0

        return np.array([self.classes[x] for x in result.astype(np.int16)])

    def score(self, X: Sequence[Sequence[float]], y: Sequence[float], **kwargs) -> float:
        """
        evaluate accuracy of estimator on given test data

        :param X: test feature vectors

        :param y: test targets

        :param kwargs: ignored (here for compabillity reasons)

        :return: a single scalar, the mean accuracy on the given data

        """
        return accuracy_score(y, self.predict(X))

    @property
    def w(self) -> np.ndarray:
        """
        Getter for weights of the classifier

        :return: weight vectors of the classifier, np.ndarray of shape(n_classes, n_dim)

        """
        if not self._fitted:
            warnings.warn("weight vector is not defined for a non-fitted mcmpy. Returning a (1,1) zero vector")
            return np.array([[0]])
        return np.array([np.sum(l * x for l, x in zip(lambdas, Xs)) for lambdas, _, Xs, _ in self.cls])

    @property
    def b(self) -> np.ndarray:
        """
        Getter for biases of the classifier

        :return: biases of the classifier, np.ndarray of shape(n_classes,)

        """
        if not self._fitted:
            warnings.warn("bias is not defined for a non-fitted mcmpy. Returning a (1,) zero vector")
            return np.array([0])
        return np.array([b for _, b, _, _ in self.cls])

    @property
    def support_vectors(self) -> np.ndarray:
        """
        Getter for the support vectors of this classifier

        :return: np.ndarray of shape (n_sv, n_features)

        """
        svs = np.array([xs for _, _, xs, _ in self.cls])
        return np.unique(svs.reshape((-1, svs.shape[2])), axis=0)

    @property
    def n_support(self) -> np.ndarray:
        """
        Getter for support vectors per class

        :return: np.ndarray of shape(n_classifiers,)

        """
        if self.n_classes == 2:
            return np.array([len(self.cls[0][2]), len(self.cls[0][2])])
        return np.array([len(xs) for _, _, xs, _ in self.cls])

    @property
    def hs(self) -> np.ndarray:
        """
        Getter for the h values of each classifier

        :return: np.ndarray of shape (n_classifiers,)

        """
        return np.array([h for _, _, _, h in self.cls])

    @property
    def lambdas(self) -> List[np.ndarray]:
        """
        Getter for the lambda values of each support vector

        :return: list of np.ndarrays

        """
        return [alps for alps, _, _, _ in self.cls]

    # noinspection PyArgumentList
    def _solve_optimization(self, X: np.ndarray, y: np.ndarray) -> Tuple[np.ndarray, float, np.ndarray, float]:
        """
        train classifier on given data, use gurobi framework

        :param X: feature vectors, 2d np.array

        :param y: target vector, 1d np array

        :return: tuple of (lambdas, b, support, h) with lambdas being a 1d np.array of length n_samples, b being a float,

        X being the support vectors as np.ndarray

        """
        from gurobipy import Model, GRB, quicksum

        # create model
        model = Model("mcmpy")

        # set parameters
        model.setParam(GRB.param.OutputFlag, int(self.verbose > 1))
        model.setParam(GRB.param.LogFile, "gurobi.log" if self.verbose > 1 else "")
        model.setParam(GRB.param.LogToConsole, int(self.verbose > 1))
        model.setParam(GRB.param.BarIterLimit, self.maxiter)
        model.setParam(GRB.param.IterationLimit, self.maxiter)

        assert 1e-9 <= self.tol <= 1e-6, "Invalid value for tol: must be between 1e-6 and 1e-9"
        model.setParam(GRB.param.FeasibilityTol, self.tol)
        model.setParam(GRB.param.BarConvTol, self.tol)
        model.setParam(GRB.param.OptimalityTol, self.tol)
        model.setParam(GRB.param.Method, self.algorithm)

        # add variables
        h = model.addVar(name='h', lb=1)

        slack = model.addVars(['q{}'.format(i) for i, _ in enumerate(X)], lb=0)
        lambdas = model.addVars(['l{}'.format(i) for i, _ in enumerate(X)], lb=float('-inf'))

        # noinspection PyArgumentList
        b = model.addVar(name='b', lb=float('-inf'))

        # add objective, equation (23)
        obj = h + self.C * slack.sum()
        model.setObjective(obj, sense=GRB.MINIMIZE)
        model.update()

        # add constraints
        for i, (X_i, y_i) in enumerate(zip(X, y)):
            # first constraint, equation (24)
            lhs = h
            sense = GRB.GREATER_EQUAL
            assert len(lambdas) == len(X), "lambdas and X have different length!"

            rhs = y_i * (quicksum(
                lambdas[lamb] * self.K(X_i, X[j]) for j, lamb in enumerate(lambdas)
            ) + b) + slack['q{}'.format(i)]

            model.addConstr(lhs=lhs, sense=sense, rhs=rhs, name='c1.{}'.format(i))

            # second constraint, equation (25)
            lhs = y_i * (quicksum(
                lambdas[lamb] * self.K(X_i, X[j]) for j, lamb in enumerate(lambdas)
            ) + b) + slack['q{}'.format(i)]

            sense = GRB.GREATER_EQUAL
            rhs = 1
            model.addConstr(lhs=lhs, sense=sense, rhs=rhs, name='c2.{}'.format(i))

        model.update()
        model.optimize()

        if self.verbose > 0:
            status = model.getAttr('Status')
            print("Optimization terminated with code {}".format(status))


        # save optimized lambdas and bias
        try:
            lambdas_out = np.array([l.x for l in np.squeeze(lambdas.values())])
            b_out = b.x
            h_final = h.x
        except AttributeError:
            # solver could not find an optimal solution
            lambdas_out = np.array([0 for _ in np.squeeze(lambdas.values())])
            b_out = 0
            h_final = np.nan
            if self.verbose > 0:
                warnings.warn("The solver was not able to find a feasible solution. The usual reason for this are "
                              "numerical issues. This can often be solved by increasing C, gamma or tol!", RuntimeWarning)

        not_zero = np.nonzero(lambdas_out)
        return lambdas_out[not_zero], b_out, X[not_zero], h_final

    def _binarize(self, X: np.ndarray, y: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """
        creates n_classes binary classification problems from input data by one vs rest approach

        :param X: feature vectors

        :param y: target vector

        :return: Tuple of (Xs, ys), equal-indexed as the n binarized problems

        """
        if self.n_classes == 2:
            y = y.copy()
            y[y == 0] = -1
            return np.array([X]), np.array([y])

        elif self.multiclass == "ovr":
            ys = np.array([(y == c).astype(int) for c in range(self.n_classes)])
            ys[ys == 0] = -1
            return np.array([X] * self.n_classes), ys

        elif self.multiclass == "ovo":
            Xs = []
            ys = []
            self.ovo_combis = list(combinations(range(self.n_classes), 2))
            cdata = np.concatenate((X, np.array([y]).T), axis=1)
            for c1, c2 in self.ovo_combis:
                sel_data = cdata[np.isin(cdata[:, -1], [c1, c2])]
                Xs.append(sel_data[:, :-1])
                y_t = sel_data[:, -1]
                y_t = np.array([1 if x == c1 else -1 for x in y_t])
                ys.append(y_t)
            return np.array(Xs), np.array(ys)

        else:
            raise ValueError("binarization strategy not understood: {}".format(self.multiclass))

    def _vote_binary(self, X: np.ndarray) -> np.ndarray:
        """
        evaluate the single classifier trained for a binary problem

        :param X: feature vectors

        :return: np.array of length len(X) with predicted class labels

        """
        assert len(self.cls) == 1
        lambdas, b, X_s, _ = self.cls[0]
        cls = np.array([
            np.sign(np.sum([l_j * self.K(X_i, x_j) for l_j, x_j in zip(lambdas, X_s)]) + b) for X_i in X
        ])
        if 0 in cls:
            cls[cls == 0] = 1
            if all(cls == 0) and self.verbose > 0:
                warnings.warn("WARNING: All predicted samples lie on the decision line. Something went terribly wrong!")
        return cls

    def _vote_ovr(self, X: np.ndarray):
        """
        evaluate the n ovr classifiers

        :param X: feature vectors

        :return: np.ndarray of len(X) with predicted class labels

        """
        scores = [
            [
                (np.sum([l_j * self.K(X_i, x_j) for l_j, x_j in zip(lambdas, X_s)]) + b)
                for lambdas, b, X_s, _ in self.cls
            ]
            for X_i in X
        ]
        return np.array([np.argmax(x) for x in scores])

    def _vote_ovo(self, X: np.ndarray):
        """
        evaluate ovo classifiers

        :param X: feature vectors

        :return: np.ndarray of len(X) with predicted class labels

        """
        assert self.ovo_combis is not None, "self.ovo_combis is None"
        scores = [[0 for _ in range(self.n_classes)] for _ in X]
        for i, X_i in enumerate(X):
            for (lambdas, b, X_s, _), (c1, c2) in zip(self.cls, self.ovo_combis):
                sc = (np.sum(
                    [l_j * self.K(X_i, x_j) for l_j, x_j in zip(lambdas, X_s)]) + b)
                scores[i][c1] += sc
        return np.array([np.argmax(x) for x in scores])

    def _map_classes(self, y: np.ndarray, train=True) -> np.ndarray:
        """
        maps class labels to int. If train == True, also sets the class mapping in the field self.classes

        :param y: target vector

        :param train: if True, set self.classes, else just do the remapping

        :return: mapped target vector of dtype int16

        """
        if train:
            self.classes = np.unique(y)
        y_remaped = np.fromiter(map(lambda x: np.where(self.classes == x)[0].item(), y), dtype=np.int16)
        return y_remaped

    def _kernel_linear(self, p: np.ndarray, q: np.ndarray) -> float:
        """
        :param p: feature vector

        :param q: support vector

        :return: dot product of p, q as float

        """
        return p.dot(q).item()

    def _kernel_poly(self, p: np.ndarray, q: np.ndarray) -> float:
        """
        :param p: feature vector

        :param q: support vector

        :return: polynomial kernel of p, q as float

        """
        return np.power(self._kernel_linear(p, q) + 1, self.degree)

    def _kernel_gauss(self, p: np.ndarray, q: np.ndarray) -> float:
        """
        :param p: feature vector

        :param q: support vector

        :return: gaussian kernel of p, q as float

        """
        return np.exp(-np.power(np.linalg.norm(p - q, 2), 2) * self.gamma)

    def _kernel_sigmoid(self, p: np.ndarray, q: np.ndarray) -> float:
        """
        :param p: feature vector

        :param q: support vector

        :return: sigmoid kernel of p, q as float

        """
        return np.tanh(self.gamma * self._kernel_linear(p, q) + self.coef0)

# NEXT DATASET
formatted_datasets = ["australian.dat", "haberman.data", "heart.dat",
                      "transfusion.data", "glass.data", "hayes-roth.data", "tae.data"]

import pandas as pd
from os.path import join

for ds in formatted_datasets:
    ds_name = ds.split('.')[0]
    df = pd.read_csv(join('../data', ds), sep=None, header=None)
    df.fillna(df.mean(), inplace=True)
    df.to_hdf('processed.h5', key=ds_name)

print("formatted datasets done")
# NEXT DATASET
df = pd.read_csv("../data/blogger.csv", sep=';', dtype='category')

map = {
    'high': 2,
    'medium': 1,
    'low': 0,
    'left': -1,
    'middle': 0,
    'right': 1,
    'yes': 1,
    'no': -1
}
map_fct = lambda x: map[x] if x in map else x

df = df.applymap(map_fct)

oh = pd.get_dummies(df['topic'], prefix='topic', drop_first=True)
df.drop('topic', inplace=True, axis=1)
df = df.join(oh)

cols = df.columns.tolist()
cols.remove('pb')
cols.append('pb')
df = df[cols]
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="blogger")

print("blogger done")
# NEXT DATASET
df = pd.read_csv("../data/fertility.txt", header=None)
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="fertility")
print("fertility done")
# NEXT DATASET
from sklearn.preprocessing import LabelEncoder

df = pd.read_csv("../data/promoters.data", header=None)
feat = df.iloc[:, 2]
labels = df.iloc[:, 0]

feat = feat.apply(lambda s: pd.Series(list(s.strip())))
enc = LabelEncoder()
enc.fit(['a', 'g', 't', 'c'])
feat = feat.apply(enc.transform)
df = feat.join(labels, rsuffix='lbl')
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="promoters")
print("blogger done")
# NEXT DATASET
df = pd.read_csv("../data/echocardiogram.data", header=None, sep=',', na_values=["?"], error_bad_lines=False)
df = df[pd.notnull(df[12])]
df.drop(10, axis=1, inplace=True)
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="echocardiogram")
print("echocardiogram done")
# NEXT DATASET
df = pd.read_csv("../data/hepatitis.data", header=None, sep=',', na_values=["?"])
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="hepatitis")
print("hepatitis done")
# NEXT DATASET
df = pd.read_csv("../data/plrx.txt", header=None, sep="\t")
df.drop(13, axis=1, inplace=True)
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="plrx")
print("plrx done")
# NEXT DATASET
df = pd.read_csv("../data/seeds_dataset.txt", header=None, sep="\t+")
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="seeds")
print("seeds done")
# NEXT DATASET
df1 = pd.read_csv("../data/horse-colic.data", header=None, delim_whitespace=True, na_values=["?"])
df2 = pd.read_csv("../data/horse-colic.test", header=None, delim_whitespace=True, na_values=["?"])
df1.fillna(df1.mean(), inplace=True)
df2.fillna(df2.mean(), inplace=True)
df1.to_hdf('processed.h5', key="horsecolic_train")
df1.to_hdf('processed.h5', key="horsecolic_test")
print("horsecolic done")
# NEXT DATASET
df = pd.read_csv("../data/ecoli.data", header=None, delim_whitespace=True)
df.drop(0, axis=1, inplace=True)
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="ecoli")
print("ecoli done")
# NEXT DATASET
df = pd.read_csv("../data/house_voters-84.data", header=None, na_values=["?"])
map = {
    'y': 1,
    'n': -1,
}
df = df.applymap(map_fct)
cols = df.columns.tolist()
cols.remove(0)
cols.append(0)
df = df[cols]
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="house_voters")
print("voters done")
# NEXT DATASET
df = pd.read_csv("../data/ilpd.csv", header=None)
map = {
    'Female': 1,
    'Male': -1
}
df = df.applymap(map_fct)
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="ilpd")
print("ilpd done")
# NEXT DATASET
df = pd.read_csv("../data/balance-scale.data", header=None)
cols = df.columns.tolist()
cols.remove(0)
cols.append(0)
df = df[cols]
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="balance_scale")
print("balance scale done")
# NEXT DATASET
df = pd.read_csv("../data/crx.data", header=None, na_values=["?"])
map = {
    't': 1,
    'f': -1,
}

df = df.applymap(map_fct)
labels = df.iloc[:, -1]
df = df.drop(len(df.columns.tolist()) - 1, axis=1)
one_hots = [0, 3, 4, 5, 6, 12]
for oh in one_hots:
    coded = pd.get_dummies(df[oh], prefix=oh, drop_first=True)
    df.drop(oh, axis=1, inplace=True)
    df = df.join(coded)

df = df.join(labels)

df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="crx")
print("crx done")
# NEXT DATASET
df = pd.read_csv("../data/tic-tac-toe.data", header=None)
map = {
    'x': 1,
    'b': 0,
    'o': -1
}

df = df.applymap(map_fct)
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="tic-tac-toe")
print("tic-tac-toe done")
# NEXT DATASET
df_features = pd.read_csv("../data/secom.data", header=None, sep=None)
df_labels = pd.read_csv("../data/secom_labels.data", header=None, sep=None)[0]
df = df_features.join(df_labels, rsuffix='lbl')
df.dropna(axis=1, inplace=True)
df.to_hdf('processed.h5', key="secom")
print("secom done")
# NEXT DATASET
df = pd.read_csv("../data/Wholesale customers data.csv", header=None)
cols = df.columns.tolist()
cols.remove(0)
cols.append(0)
df = df[cols]
df.fillna(df.mean(), inplace=True)
df.to_hdf('processed.h5', key="Wholesale customers data")
print("Wholesale done")


import setuptools

with open("README.md", "r") as fh:

    long_description = fh.read()

setuptools.setup(
    name='mcmpy',
    version='1.3.3',
    packages=['mcmpy'],
    author="Benedikt Franke",
    author_email="benedikt.franke@uni-ulm.de",
    description="A python implementation of the Minimum Complexity Machine",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/BenediktFranke/bachelorthesis",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'numpy',
        'sklearn'
    ],
 )

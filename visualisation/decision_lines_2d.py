from matplotlib import pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC

from mcmpy import MCMClassifier
from tikzplotlib import save
from sys import argv

seed = 42
if len(argv) > 1:
        seed = int(argv[1])

np.random.seed(seed)
x_1 = np.random.normal(loc=-2.2, size=(500,), scale=1)
x_2 = np.random.normal(loc=2.2, size=(500,), scale=1)

X_1 = np.hstack((x_1.reshape(-1, 1), np.array([[1.1]]*500)))
X_2 = np.hstack((x_2.reshape(-1, 1), np.array([[0.9]]*500)))
X = np.vstack((X_1, X_2))
y = np.array(([0] * 500) + ([1] * 500))

X_train, X_test, y_train, y_test = train_test_split(X, y, shuffle=True, test_size=0.8, random_state=seed)

x_limits = [int(np.min(X[:, 0])) - 1, int(np.max(X[:, 0])) + 1]

mcm = MCMClassifier(C=100, kernel='linear').fit(X_train, y_train)

w1_mcm, w2_mcm = np.squeeze(mcm.w)
b_mcm = mcm.b

x_line_mcm = np.fromiter(range(*x_limits), dtype=np.int32)
y_line_mcm = np.fromiter(((-w1_mcm*x - b_mcm)/w2_mcm for x in x_line_mcm), dtype=np.float32)

svm = SVC(C=100, kernel='linear')
svm.fit(X_train, y_train)

w1_svm, w2_svm = svm.coef_[0]
b_svm = svm.intercept_

x_line_svm = np.fromiter(range(*x_limits), dtype=np.int32)
y_line_svm = np.fromiter(((-w1_svm * x - b_svm) / w2_svm for x in x_line_svm), dtype=np.float32)


fig, (ax1, ax2) = plt.subplots(1, 2)

ax1.scatter(X_test[:, 0], X_test[:, 1], c='blue', marker='x', s=6, label="test data")
ax1.scatter(X_train[:, 0], X_train[:, 1], c='red', marker='.', s=6, label="training data")
ax1.plot(x_line_mcm, y_line_mcm)

ax1.set_ylim([0.8,1.2])
ax1.set_xlim(x_limits)

ax2.scatter(X_test[:, 0], X_test[:, 1], c='blue', marker='x', s=6, label="test data")
ax2.scatter(X_train[:, 0], X_train[:, 1], c='red', marker='.', s=6, label="training data")


ax2.plot(x_line_svm, y_line_svm)

ax2.set_ylim([0.8, 1.2])
ax2.set_xlim(x_limits)

ax1.grid(True)
ax2.grid(True)

plt.legend()
save("SVMvsMCM.tex")
#plt.savefig("SVMvsMCM.pdf"
#plt.show()

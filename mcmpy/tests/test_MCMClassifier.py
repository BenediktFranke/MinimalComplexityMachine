from unittest import TestCase

from sklearn.model_selection import train_test_split

from mcmpy.MCM import MCMClassifier
from sklearn.datasets import make_blobs
import numpy as np
from numpy.testing import assert_array_equal


class TestMCMClassifier(TestCase):
    def test_predict(self):
        mcm = MCMClassifier()
        mcm.n_classes = 2
        mcm.classes = np.array([0,1])
        lambdas = np.array([1, 1, -1, -1])
        support = np.array([[1,1], [3,1], [1,5], [3,5]])
        b = 24
        mcm.cls = np.array([(lambdas, b, support, 5)])
        mcm._fitted = True
        mcm.K = mcm._kernel_linear
        X = np.array([[5, 6], [-10, 2], [5, -9], [-3, -4]])
        y_exp = np.array([0, 1, 1, 1])
        y_act = mcm.predict(X)
        assert_array_equal(y_exp, y_act)

    def test_score(self):
        mcm = MCMClassifier()
        mcm.n_classes = 2
        mcm.classes = np.array([0,1])
        lambdas = np.array([1, 1, -1, -1])
        support = np.array([[1, 1], [3, 1], [1, 5], [3, 5]])
        b = 24
        mcm.cls = np.array([(lambdas, b, support, 5)])
        mcm._fitted = True
        mcm.K = mcm._kernel_linear
        X = np.array([[5, 6], [10, 2], [5, 9], [-3, -4]])
        y = np.array([1, 1, 1, 1])
        acc_exp = 0.5
        acc_act = mcm.score(X, y)
        self.assertEqual(acc_exp, acc_act)

    def test_fit(self):
        mcm = MCMClassifier(verbose=1, kernel='linear')

        X, y = make_blobs(50, 2, 2, random_state=42)
        mcm.fit(X, y)
        self.assertEqual(1.0, mcm.score(X, y))

    def test_fit_3d(self):
        mcm = MCMClassifier(verbose=1, maxiter=100000)

        X, y = make_blobs(50, 3, 2, random_state=42)
        mcm.fit(X, y)
        self.assertEqual(1.0, mcm.score(X, y))

    def test_fit_3d_artificial(self):
        mcm = MCMClassifier(verbose=1, maxiter=100000)

        X = [[1, 100, 10], [100, 1, 11], [77, 77, 9],
                      [20, 6, 0], [33, 33, 1], [100, 100, -1]]
        y = [1, 1, 1, 0, 0, 0]

        mcm.fit(X, y)
        self.assertEqual(1.0, mcm.score(X, y))

    def test_fit_line(self):
        mcm = MCMClassifier(tol=1.0e-9, maxiter=10000000, C=100)
        pos = [[x, 0.51] for x in np.arange(0, 20, 0.2)]
        neg = [[x, 0.49] for x in np.arange(3, 23, 0.2)]
        X = np.array(pos + neg)
        y = np.array([1 for _ in pos] + [0 for _ in neg])
        X_train, X_test, y_train, y_test = train_test_split(X, y, shuffle=True, random_state=1)
        mcm.fit(X_train, y_train)
        self.assertEqual(1.0, mcm.score(X_test, y_test))

    def test_kernel_linear(self):
        mcm = MCMClassifier()
        p = np.array([1, 2])
        q = np.array([3, 4])
        exp = 11
        act = mcm._kernel_linear(p, q)
        self.assertEqual(exp, act)

    def test_kernel_poly(self):
        mcm = MCMClassifier(gamma=2)
        p = np.array([1, 2])
        q = np.array([3, 4])
        exp = 144
        act = mcm._kernel_poly(p, q)
        self.assertEqual(exp, act)

    def test_kernel_gauss(self):
        mcm = MCMClassifier()
        mcm.gamma = 1
        q = np.array([2, 3])
        p = np.array([3, 3])
        exp = np.exp(-1)
        act = mcm._kernel_gauss(p, q)
        self.assertAlmostEqual(exp, act, delta=1e-6)

    def test_kernel_sigmoid(self):
        mcm = MCMClassifier(gamma=2, coef0=-8)
        p = np.array([1, 2])
        q = np.array([2, 1])
        exp = 0
        act = mcm._kernel_sigmoid(p, q)
        self.assertAlmostEqual(exp, act, delta=1e-6)

    def test_bin_bin(self):
        mcm = MCMClassifier()
        mcm.n_classes = 2
        mcm.classes = np.array([0, 1])
        X = np.array([[5,6], [7,8], [9,10]])
        y = np.array([1,0,1])
        _, y_act = mcm._binarize(X,y)
        y_exp = np.array([[1, -1, 1]])
        assert_array_equal(y_exp, y_act)

    def test_bin_ovr(self):
        mcm = MCMClassifier(multiclass='ovr')
        mcm.n_classes = 3
        mcm.classes = np.array([0,1,2])
        X = np.array([[5,6], [7,8], [9,10]])
        y = np.array([1,0,2])
        Xs_act, ys_act = mcm._binarize(X, y)
        Xs_exp = np.array([[[5,6], [7,8], [9,10]]] * 3)
        ys_exp = np.array([[-1,1,-1], [1, -1, -1], [-1, -1, 1]])

        assert_array_equal(Xs_exp, Xs_act)
        assert_array_equal(ys_exp, ys_act)

    def test_bin_ovo(self):
        mcm = MCMClassifier(multiclass='ovo')
        mcm.n_classes = 3
        mcm.classes = np.array([0,1,2])
        X = np.array([[5, 6], [7, 8], [9, 10]])
        y = np.array([1, 0, 2])
        Xs_act, ys_act = mcm._binarize(X, y)

        Xs_exp = np.array([[[5, 6], [7, 8]], [[7, 8], [9, 10]], [[5,6], [9,10]]])
        ys_exp = np.array([[-1, 1], [1, -1], [1, -1]])

        assert_array_equal(Xs_exp, Xs_act)
        assert_array_equal(ys_exp, ys_act)

    def test_vote_binary(self):
        mcm = MCMClassifier()
        mcm.n_classes = 2
        lambdas = np.array([1, 1, -1, -1])
        support = np.array([[1, 1], [3, 1], [1, 5], [3, 5]])
        b = 24
        mcm.cls = np.array([(lambdas, b, support, 5)])
        mcm._fitted = True
        mcm.K = mcm._kernel_linear
        X = np.array([[5, 6], [10, 2], [5, 9], [-3, -4]])
        y_exp = np.array([-1, 1, -1, 1])

        res = mcm._vote_binary(X)

        assert_array_equal(y_exp, res)

    def test_vote_ovr(self):
        mcm = MCMClassifier()
        mcm.n_classes = 3
        mcm.K = mcm._kernel_linear
        support = np.array([[1, 0], [0, 1], [4, 5]])

        lambdas1 = np.array([0, -1, 0])
        b1 = 3

        lambdas2 = np.array([-2, 1, 0])
        b2 = 0

        lambdas3 = np.array([1, 1, 0])
        b3 = -9

        mcm.cls = [(lambdas1, b1, support, 5), (lambdas2, b2, support, 5), (lambdas3, b3, support, 5)]
        X = np.array([[1, 5], [3, 1], [5, 5]])
        y_exp = np.array([1, 0, 2])

        assert_array_equal(y_exp, mcm._vote_ovr(X))

    def test_vote_ovo(self):
        mcm = MCMClassifier()
        mcm.n_classes = 3
        mcm.K = mcm._kernel_linear
        mcm.ovo_combis = [(1,0), (0,2), (1,2)]
        support = np.array([[1, 0], [0, 1], [4, 5]])

        # class 1 vs 0
        lambdas1 = np.array([-1, 1, 0])
        b1 = 0
        # class 0 vs 2
        lambdas2 = np.array([-1, -1, 0])
        b2 = 6
        # class 1 vs 2
        lambdas3 = np.array([-1, 0, 0])
        b3 = 3

        mcm.cls = [(lambdas1, b1, support, 5), (lambdas2, b2, support, 5), (lambdas3, b3, support, 5)]
        X = np.array([[1, 5], [3, 1], [5, 5]])
        y_exp = np.array([1, 0, 2])

        assert_array_equal(y_exp, mcm._vote_ovo(X))

    def test_fit_error(self):
        mcm = MCMClassifier(kernel="foo")
        with self.assertRaises(ValueError):
            mcm.fit([[1],[2],[3]], [8,9,10])

    def test_predict_unfitted(self):
        mcm = MCMClassifier()
        with self.assertRaises(RuntimeError):
            mcm.predict([[1],[2],[3]])

    def test_bin_error(self):
        mcm = MCMClassifier(multiclass="ba")
        mcm._fitted = True
        with self.assertRaises(ValueError):
            mcm.predict([[1],[2],[3]])

    def test_w_unfitted(self):
        mcm = MCMClassifier()
        assert_array_equal(np.array([[0]]), mcm.w)

    def test_b_unfitted(self):
        mcm = MCMClassifier()
        self.assertEqual(0, mcm.b)

    def test_fit_ovo(self):
        mcm = MCMClassifier(verbose=1, multiclass='ovo', gamma=5)

        X, y = make_blobs(50, 2, 3, random_state=42)

        mcm.fit(X, y)
        self.assertEqual(1.0, mcm.score(X, y))

    def test_fit_ovr(self):
        mcm = MCMClassifier(verbose=1, multiclass='ovr')

        X, y = make_blobs(50, 2, 3, random_state=42)

        mcm.fit(X, y)
        self.assertEqual(1.0, mcm.score(X, y))

    def test_fit_poly(self):
        mcm = MCMClassifier(verbose=1, kernel='poly', gamma=2)

        X, y = make_blobs(50, 2, 2, random_state=42)
        mcm.fit(X, y)
        self.assertEqual(1.0, mcm.score(X, y))

    def test_fit_gauss(self):
        mcm = MCMClassifier(verbose=1, kernel='gauss')

        X, y = make_blobs(50, 2, 2, random_state=42)
        mcm.fit(X, y)
        self.assertEqual(1.0, mcm.score(X, y))

    def test_fit_sig(self):
        mcm = MCMClassifier(verbose=1, kernel='sigmoid')

        X, y = make_blobs(50, 2, 2, random_state=42)
        mcm.fit(X, y)
        self.assertEqual(1.0, mcm.score(X, y))

    def test_fit_function(self):
        mcm = MCMClassifier(verbose=1, kernel=lambda x, y: np.dot(x, y))

        X, y = make_blobs(50, 2, 2, random_state=42)
        mcm.fit(X, y)
        self.assertEqual(1.0, mcm.score(X, y))

    def test_missing_classes(self):
        X = np.array([[10, 10], [1, 1], [9, 11], [0,1]])
        y = np.array([0, 2, 0, 2])
        mcm = MCMClassifier()
        mcm.fit(X, y)
        self.assertEqual(1.0, mcm.score(X, y))

    def test_categorical_classes(self):
        X = np.array([[10, 10], [1, 1], [9, 11], [0,1]])
        y = np.array(['pos', 'neg', 'pos', 'neg'])
        mcm = MCMClassifier()
        mcm.fit(X, y)
        tst = mcm.score(X, y)
        self.assertEqual(1.0, tst)

    def test_categorical_classes_ovo(self):
        X = np.array([[10, 10], [1, 1], [9, 11], [0,1], [-5, 10], [-10, 5]])
        y = np.array(['pos', 'neg', 'pos', 'neg', 'neutral', 'neutral'])
        mcm = MCMClassifier(multiclass='ovo', verbose=1)
        mcm.fit(X, y)
        tst = mcm.score(X, y)
        self.assertEqual(1.0, tst)

    def test_categorical_classes_ovr(self):
        X = np.array([[10, 10], [1, 1], [9, 11], [0, 1], [-5, 10], [-10, 5]])
        y = np.array(['pos', 'neg', 'pos', 'neg', 'neutral', 'neutral'])
        mcm = MCMClassifier(multiclass='ovr', verbose=1)
        mcm.fit(X, y)
        tst = mcm.score(X, y)
        self.assertEqual(1.0, tst)

    def test_w(self):
        mcm = MCMClassifier()
        mcm.cls = np.array([(np.array([2, 3, 0]), 0, np.array([[1, 0], [0, 1]]), 0)])
        mcm._fitted = True
        w_exp = np.array([[2, 3]])
        w_act = mcm.w

        assert_array_equal(w_exp, w_act)

    def test_n_support_binary(self):
        mcm = MCMClassifier()
        X, y = make_blobs(centers=2)
        mcm.fit(X, y)
        res = mcm.n_support
        self.assertEqual(res[0], res[1])

    def test_n_support_multi(self):
        mcm = MCMClassifier()
        X, y = make_blobs(centers=4)
        mcm.fit(X, y)
        res = mcm.n_support
        self.assertEqual(len(res), 4)

    def test_support_vectors(self):
        mcm = MCMClassifier()
        X, y = make_blobs(centers=3)
        mcm.fit(X, y)
        res = mcm.support_vectors
        self.assertTrue(res.shape[0] > 0)


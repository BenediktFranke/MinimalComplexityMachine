from sklearn.datasets import load_breast_cancer, make_blobs
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
import numpy as np

from mcmpy.MCM import MCMClassifier


def ex0a():
    X, y = make_blobs(1000, 4, 2)

    X_train, X_test, y_train, y_test = train_test_split(X, y)

    cls = MCMClassifier()
    param_grid = {
        'C': np.geomspace(1.2, 1.7, 100).tolist(),
    }
    gs = GridSearchCV(cls, param_grid, n_jobs=8)

    gs.fit(X_train, y_train)

    print("----------TRAINING COMPLETE------------")
    print("test score: {}".format(gs.score(X_test, y_test)))
    print("best params: {}".format(gs.best_params_))


def ex0b():
    X, y = make_blobs(1000, 4, 2)

    X_train, X_test, y_train, y_test = train_test_split(X, y)

    cls = MCMClassifier()
    param_grid = {
        'C': np.geomspace(1.2, 1.7, 100).tolist(),
    }
    gs = GridSearchCV(cls, param_grid, n_jobs=8)

    gs.fit(X_train, y_train)

    print("----------TRAINING COMPLETE------------")
    print("test score: {}".format(gs.score(X_test, y_test)))
    print("best params: {}".format(gs.best_params_))

def ex1():
    X = np.array([[0,0], [0,1], [1,0], [1,1]] * 100)
    y = np.array([0, 1, 1, 0] * 100)


    cls = MCMClassifier()
    param_grid = {
        'C': np.geomspace(0.000001, 10, 1000).tolist(),
    }
    gs = GridSearchCV(cls, param_grid, n_jobs=8, verbose=4)

    gs.fit(X, y)

    print("----------TRAINING COMPLETE------------")
    print("test score: {}".format(gs.score(X, y)))
    print("best params: {}".format(gs.best_params_))


def ex2():
    X, y = load_breast_cancer(True)

    X_train, X_test, y_train, y_test = train_test_split(X, y)

    cls = MCMClassifier()
    param_grid = {
        'C': np.geomspace(0.00000001, 10, 100).tolist(),
    }
    gs = GridSearchCV(cls, param_grid, n_jobs=8, verbose=3)

    gs.fit(X_train, y_train)

    print("----------TRAINING COMPLETE------------")
    print("test score: {}".format(gs.score(X_test, y_test)))
    print("best params: {}".format(gs.best_params_))

def ex3():
    # make data set
    X, y = make_blobs(1000, 4, 2)
    X_train, X_test, y_train, y_test = train_test_split(X, y)

    # create MCM
    mcm = MCMClassifier(C=100, kernel="gauss", algorithm="barrier", gamma=10)

    # training
    mcm.fit(X_train, y_train)

    # accuracy evaluation
    print(mcm.score(X_test, y_test))

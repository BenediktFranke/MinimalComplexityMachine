"""!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"""
"""deprecated, only for reference"""

import numpy as np
import pandas as pd
from scipy.optimize import linprog
from matplotlib import pyplot as plt
from sklearn.datasets import make_blobs

import matplotlib

matplotlib.use('Qt5Agg')

class primitive_MCM:

    def h(self, uv, *args):
        X = args[0]
        y = args[1]
        return np.max([y_i * np.dot(uv[0], x_i) + uv[1] for x_i, y_i in zip(X, y)]) / np.min([y_i * np.dot(uv[0], x_i) + uv[1] for x_i, y_i in zip(X, y)])

    def hd(self, uv, *args):
        X = args[0]
        y = args[1]
        hdx = "TODO"
        hdy = "TODO"
        return np.array([hdx, hdy])

    def __init__(self, C):
        self.X = None
        self.y = None
        self.C = C
        self.w = None
        self.b = None
        self.m = 0

    def load_data(self, path, classname='class', format=None, *args):
        if format is None and path[-1:-3] == 'csv' or format == 'csv':
            data = pd.read_csv(path, *args)
        elif format is None and path[-1:-2] == 'h5' or format == 'hdf':
            data = pd.read_hdf(path, *args)
        else:
            raise Exception("Unknown file-format: {}".format(path))

        self.y = data[classname]
        self.X = data.drop(classname, axis=1)
        data = None

        self.X = (self.X - self.X.mean()) / self.X.std()

        self.w = np.ones(self.X.shape)
        self.b = 0
        self.m = len(self.X)

    def shuffle_data(self):
        assert len(self.X) == len(self.y)
        p = np.random.permutation(len(self.X))
        self.X = self.X[p]
        self.y = self.y[p]

    def load_dummy(self):
        """atm this loads a hard-coded example, todo add randomness and params"""
        self.X, self.y = make_blobs(1000, 2, 2)
        self.y[self.y == 0] = -1
        self.w = np.ones(2)
        self.b = 0
        self.shuffle_data()
        self.m = len(self.X)

        #self.X = (self.X - self.X.mean()) / self.X.std()

    def load_dummy2(self):
        """atm this loads a hard-coded example, todo add randomness and params"""
        pos = [[x, 0.51] for x in np.arange(0, 5, 0.23)]
        neg = [[x, 0.49] for x in np.arange(3, 8, 0.23)]
        self.X = np.array(pos + neg)
        self.y = np.array([1 for _ in pos] + [-1 for _ in neg])
        self.w = np.ones(2)
        self.b = 0
        self.shuffle_data()
        self.m = len(self.X)

        #self.X = (self.X - self.X.mean()) / self.X.std()

    def train(self):

        # set up linear programming problem
        c = np.array([1] + [0 for _ in self.X[0]] + [0])  # coefficients of h
        fst_const = [([-1] + [y*x_i for x_i in x] + [y]) for x, y in zip(self.X, self.y)]
        fst_b = [0 for _ in fst_const]

        snd_const = [([0] + [-y*x_i for x_i in x] + [-y]) for x, y in zip(self.X, self.y)]
        snd_b = [-1 for _ in snd_const]

        A = np.array(fst_const + snd_const)
        b = np.array(fst_b + snd_b)
        bounds = (None, None)

        result = linprog(c, A, b, bounds=bounds, options={'disp': True, 'maxiter': 10000, 'tol': 5.0e-11})

        print(result)

        self.plot_solutions(result.x[1:3], result.x[3])

    def plot_solutions(self, w, b):
        x1, x2 = list(zip(*self.X.tolist()))
        fig = plt.scatter(x1, x2, color=['red' if y == 1 else 'blue' for y in self.y])
        x_line = range(-9, 9)
        y_line = [-(w[0]*x + b)/w[1] for x in x_line]
        plt.plot(x_line, y_line, "g-")
        plt.show()



if __name__ == "__main__":
    tst = primitive_MCM(1)
    tst.load_dummy2()
    tst.train()

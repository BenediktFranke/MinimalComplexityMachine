from sklearn.datasets import make_circles
from tikzplotlib import save
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot as plt
import numpy as np

fig = plt.figure(figsize=plt.figaspect(.5))

# First subplot
ax = fig.add_subplot(1, 2, 1)

X, y = make_circles(n_samples=400, factor=.3, noise=.05)
reds = y == 0
blues = y == 1

ax.scatter(X[reds, 0], X[reds, 1], c="red",
            s=20, edgecolor='k', label="class 0")
ax.scatter(X[blues, 0], X[blues, 1], c="blue",
            s=20, edgecolor='k', label="class 1")

ax.grid(True)
ax.legend()

# Second subplot
ax = fig.add_subplot(1, 2, 2, projection='3d')

ax.scatter(X[reds, 0], X[reds, 1], (X[reds, 0]**2 + X[reds, 1]**2), c="red",
            s=20, edgecolor='k', label="class 0")
ax.scatter(X[blues, 0], X[blues, 1], (X[blues, 0]**2 + X[blues, 1]**2), c="blue",
            s=20, edgecolor='k', label="class 1")

ax.grid(True)
plt.savefig("kernel.pdf", dpi=300)

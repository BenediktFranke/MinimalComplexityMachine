import json
import os
import warnings

import numpy as np
import pandas as pd
from scipy.stats import expon
from sklearn.exceptions import DataConversionWarning
from sklearn.model_selection import RandomizedSearchCV, cross_validate, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from logging import basicConfig, info, INFO

from mcmpy import MCMClassifier

warnings.filterwarnings(action='ignore', category=DataConversionWarning)
basicConfig(filename="debug.log", level=INFO, format='%(asctime)s %(message)s')

"""
The following tries to reproduce the workflow presented in https://link.springer.com/content/pdf/10.1007%2Fs11063-018-9793-9.pdf
"""


def eval_formatted(model, linear):

    res_file = "results_svm" if model == "svm" else "results_mcm"
    res_file += '_linear.h5' if linear else '.h5'

    done = None
    if os.path.exists(res_file):
        done = pd.read_hdf(res_file)

    res = pd.DataFrame(columns=['dataset', 'acc_mean', 'acc_std', 'hs_mean', 'hs_std', 'sv_mean', 'sv_std', 'params'],
                       dtype='object')
    store = pd.HDFStore('processed.h5', 'r')
    # reversed so wholesale comes last
    for ds in store.keys()[::-1]:
        ds = ds[1:] if ds[0] == '/' else ds
        info("started working on {}".format(ds))
        if done is not None and ds in done['dataset']:
            print("{} is already done!".format(ds))
            info("results existed, skipping!")
            continue
        print("\n\n")
        print("--------------------------------------------------")
        print("working on {}".format(ds))

        df = pd.read_hdf('processed.h5', key=ds)

        if ds == "horsecolic_train":
            df2 = pd.read_hdf('processed.h5', key="horsecolic_test")
            df = pd.concat((df, df2), ignore_index=True)

        elif ds == "horsecolic_test":
            continue

        X = df.iloc[:, :-1].values
        y = df.iloc[:, -1].values

        if model == 'mcm':
            grid = {
                'cls__C': np.arange(5, 150, 0.5).tolist(),
                'cls__kernel': ['gauss'],
                'cls__gamma': np.arange(1, 30, 0.5),
                'cls__tol': [1e-9, 1e-7, 1e-6],
                'cls__coef0': [0],
                'cls__multiclass': ['ovr'],
                'cls__verbose': [1],
                'cls__algorithm': ['barrier']
            }
            if linear:
                grid['cls__kernel'] = ['linear']
                grid['cls__gamma'] = [0]
        else:
            grid = {
                'cls__C': expon(scale=100),
                'cls__kernel': ['rbf'],
                'cls__gamma': ['auto', 'scale'],
                'cls__verbose': [1]
            }
            if linear:
                grid['cls__kernel'] = ['linear']
                grid['cls__gamma'] = [0]
        info("started training...")
        results = train_test(X, y, grid, model, ds)
        res = res.append(results, ignore_index=True)

        print("DONE: accuracy={}".format(results['acc_mean']))
        info("DONE: accuracy={}".format(results['acc_mean']))

        res.to_hdf(res_file, key='results')


def train_test(X, y, grid, model, dataset_name):

    if model == "mcm":
        pipe = Pipeline([('scale', StandardScaler()), ('cls', MCMClassifier(verbose=0))])
    else:
        pipe = Pipeline([('scale', StandardScaler()), ('cls', SVC(verbose=0))])

    search = RandomizedSearchCV(pipe, grid, n_jobs=6, verbose=2, cv=4, random_state=42, n_iter=200)
    results = cross_validate(search, X, y, cv=5, n_jobs=1, return_estimator=True, verbose=1)

    scores = results['test_score']
    ests = [s.best_estimator_.named_steps['cls'] for s in results['estimator']]

    param_sets = [e.best_params_ for e in results['estimator']]
    hs = np.array([e.hs for e in ests]) if model == "mcm" else np.array([np.nan for e in ests])
    sv = np.array([e.n_support for e in ests]) if model == "mcm" \
        else np.array([e.n_support_ for e in ests])

    ret = {'dataset': dataset_name,
           'acc_mean': scores.mean(),
           'acc_std': scores.std(),
           'hs_mean': hs.mean(),
           'hs_std': hs.std(),
           'sv_mean': sv.mean(),
           'sv_std': sv.std(),
           'params': json.dumps(param_sets)}

    return ret


if __name__ == "__main__":
    info("----- KERNEL MCM --------")
    eval_formatted("mcm", False)
    info("----- LINEAR MCM --------")
    eval_formatted("mcm", True)
    # info("----- KERNEL SVM --------")
    # eval_formatted("svm", False)
    # info("----- LINEAR SVM --------")
    # eval_formatted("svm", True)
